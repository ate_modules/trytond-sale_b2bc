# -*- coding: utf-8 -*-
# This file is part of Sale B2BC Tryton Module.  The 
# COPYRIGHT file at the top level of this repository contains the
# full copyright notices and license terms.

from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, Bool, Not, If

__all__ = ['SaleShop']

_PRICE_TYPE = [
    ('both', 'Tax included & Tax excluded'),
    ('t_inc', 'Tax included only'),
    ('t_exc', 'Tax excluded only'),
    ]


class SaleShop(ModelSQL, ModelView):
    __name__ = 'sale.shop'
    
    price_type = fields.Selection(_PRICE_TYPE, 'Price type',
        states={
            'required': Bool(Eval('context', {}).get('company', 0)),
            },
        on_change=['price_type'],
        help='Type of price authorized on this shop.')
    
    @classmethod
    def __setup__(cls):
        super(SaleShop, cls).__setup__()
        
        if not cls.party.domain:
            cls.party.domain = []
        if 'price_type' not in cls.party.domain:
            cls.party.domain.append((
                'price_type',
                'in',
                If(Eval('price_type', {}).contains('both'),
                   ['t_inc', 't_exc', None], [Eval('price_type')])
            ))
        #cls.party.context.update(
        #    {'tax_included': Bool(Eval('price_type', 't_exc') == 't_inc')})
         
    @staticmethod
    def default_price_type():
        return 'both'
    
    def on_change_price_type(self):
        '''
        Reset the party if changing
        '''
        return {'party': False}
        
# -*- coding: utf-8 -*-
# This file is part of Sale B2BC module for Tryton.  The 
# COPYRIGHT file at the top level of this repository contains the
# full copyright notices and license terms.

import logging
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, fields
from trytond import backend
from trytond.transaction import Transaction
from trytond.pyson import Eval, Bool, Not
from trytond.pool import Pool, PoolMeta

__all__ = ['Template', 'Product']
__metaclass__ = PoolMeta

DEPENDS = ['active','tax_included','list_price_texc']
_ZERO = Decimal('0.0')
STATES = {
    'readonly': ~Eval('active', True),
    }

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class Template(ModelSQL, ModelView):
    __name__ = "product.template"
   
    tax_included = fields.Boolean('Price Tax included',
        help='If checked you must define a price with tax included.')
    list_price_texc = fields.Property(fields.Numeric('List Price (Tax excl.)',
        states=STATES, digits=(16, 4), depends=DEPENDS, required=True,
        on_change_with=['tax_included', 'list_price_tinc', 'taxes_category',
            'category', 'customer_taxes'],
        ))
    list_price_tinc = fields.Property(fields.Numeric('List Price (Tax inc.)',
        digits=(16, 4), depends=DEPENDS,
        states={
            'required': Bool(Eval('tax_included', False)),
            'invisible': ~Eval('tax_included', False),
            },
        ))
    list_price = fields.Function(fields.Property(fields.Numeric('List Price',
        digits=(16, 4), depends=DEPENDS,
        )), 'get_list_price')
    
#     @classmethod
#     def __register__(cls, module_name):
#         TableHandler = backend.get('TableHandler')
#         cursor = Transaction().cursor
#         sql_table = cls.__table__()
#         
#         super(Template, cls).__register__(module_name)
#         
#         offset = 0
#         limit = cursor.IN_MAX
#         templates = True
#         Tpl = Pool().get('product.template')
#         while templates:
#             templates = cls.search([], offset=offset, limit=limit)
#             offset += limit
#            
#             for template in templates:
#                 test = Tpl(template)
#                 print template, template.id, test.cost_price, test.name
#                 Tpl.write([template], {'list_price_texc': '12'})

    @staticmethod
    def default_tax_included():
        return False

    @staticmethod
    def default_list_price_tinc():
        return _ZERO
    
    @staticmethod
    def default_list_price_texc():
        return _ZERO
    
    def get_list_price(self, name):    
        field_name = 'list_price_texc'
        if Transaction().context.get('tax_included', False):
             field_name = 'list_price_tinc'
        return getattr(self, field_name)
    
    def get_tax_context(self):
        res = {}
        res['tax_included'] = True if self.tax_included else False
        return res
    
    def on_change_with_list_price_texc(self):
        pool = Pool()
        Tax = pool.get('account.tax')
        Product = pool.get('product.template')
        
        price = self.list_price_tinc or _ZERO
        context = self.get_tax_context()
        if context.get('tax_included', False) and self.list_price_tinc != None:
            with Transaction().set_context(context):
                taxes = Tax.compute(
                    Tax.browse(Product.get_taxes(self,'customer_taxes_used')),
                    self.list_price_tinc, '1')
            if taxes:
                price = _ZERO
            for tax in taxes:
                price += tax['base']
        return price

class Product:
    __name__ = 'product.product'

    @classmethod
    def __setup__(cls):
        super(Product, cls).__setup__()
        cls._error_messages.update({
            'list_price_with_tax_included_required': (
                "This product '%(product)s' doesn't have 'List price' with tax "
                "included, you must define one for use it in a business "
                "transaction with prices with taxes included."),
            })

    @classmethod
    def get_sale_price(cls, products, quantity=0):
        '''
        Return the sale price for products and quantity.
        It uses if exists from the context:
            uom: the unit of measure
            currency: the currency id for the returned price
        '''
        pool = Pool()
        Uom = pool.get('product.uom')
        User = pool.get('res.user')
        Currency = pool.get('currency.currency')
        Date = pool.get('ir.date')

        today = Date.today()
        prices = {}

        uom = None
        if Transaction().context.get('uom'):
            uom = Uom(Transaction().context.get('uom'))

        currency = None
        if Transaction().context.get('currency'):
            currency = Currency(Transaction().context.get('currency'))

        user = User(Transaction().user)

        for product in products:
            if Transaction().context.get('tax_included', False):
                if product.tax_included:
                    prices[product.id] = product.list_price_tinc
                else:
                    cls.raise_user_error(
                        'list_price_with_tax_included_required', {
                        'product': product.name,
                    })
            else:
                prices[product.id] = product.list_price
            if uom:
                prices[product.id] = Uom.compute_price(
                    product.default_uom, prices[product.id], uom)
            if currency and user.company:
                if user.company.currency != currency:
                    date = Transaction().context.get('sale_date') or today
                    with Transaction().set_context(date=date):
                        prices[product.id] = Currency.compute(
                            user.company.currency, prices[product.id],
                            currency, round=False)
        return prices
# -*- coding: utf-8 -*-
# This file is part of Sale B2BC Tryton Module.  The
# COPYRIGHT file at the top level of this repository contains the
# full copyright notices and license terms.

import logging
from decimal import Decimal
from sql.aggregate import Count, Sum
from sql.conditionals import Coalesce
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, Bool, Not, If
from trytond.tools import reduce_ids
from trytond.transaction import Transaction
from trytond.pool import Pool

__all__ = ['Invoice', 'InvoiceLine']

_ZERO = Decimal('0.0')
_PRICE_TYPE = [
    ('t_inc', 'Tax included'),
    ('t_exc', 'Tax excluded'),
    ]

_logger = logging.getLogger(__name__)
_logger.setLevel(logging.INFO)


class Invoice(ModelSQL, ModelView):
    __name__ = 'account.invoice'

    price_type = fields.Selection(_PRICE_TYPE, 'Price type', required=True,
        states={
            'readonly': (Eval('state') != 'draft') | (Bool(Eval('lines'))),
        },
        help='Type of price of this invoice.')

    @classmethod
    def __setup__(cls):
        super(Invoice, cls).__setup__()
        # Extend the 'lines' on_change call
        if 'price_type' not in cls.lines.on_change:
            cls.lines.on_change.append('price_type')
        if 'price_type' not in cls.taxes.on_change:
            cls.taxes.on_change.append('price_type')
        if not getattr(cls.lines, 'context', False):
            cls.lines.context = {}
        cls.lines.context.update(
            {'tax_included': Bool(Eval('price_type', 't_exc') == 't_inc')})

    @staticmethod
    def default_price_type():
        return 't_exc'

    def get_tax_context(self):
        res = super(Invoice, self).get_tax_context()
        res['tax_included'] = True if self.price_type == 't_inc' else False
        return res

    def _on_change_lines_taxes(self):
        _logger.debug("[_on_change_lines_taxes] begin")

        context = self.get_tax_context()
        # If the price is tax excluded : Standard treatment ...
        if not context.get('tax_included', False):
            return super(Invoice, self)._on_change_lines_taxes()

        # ... else ...
        pool = Pool()
        Tax = pool.get('account.tax')
        InvoiceTax = pool.get('account.invoice.tax')
        Account = pool.get('account.account')
        TaxCode = pool.get('account.tax.code')
        res = {
            'untaxed_amount': Decimal('0.0'),
            'tax_amount': Decimal('0.0'),
            'total_amount': Decimal('0.0'),
            'taxes': {},
            }
        computed_taxes = {}

        context = self.get_tax_context()
        if self.lines:
            for line in self.lines:
                if (line.type or 'line') != 'line':
                    continue
                # Begin : Management of tax included
                res['total_amount'] += line.amount or 0
                # End : Management of tax included
                with Transaction().set_context(**context):
                    taxes = Tax.compute(line.taxes,
                        line.unit_price or Decimal('0.0'),
                        line.quantity or 0.0)
                for tax in taxes:
                    key, val = self._compute_tax(tax,
                        self.type or 'out_invoice')
                    if not key in computed_taxes:
                        computed_taxes[key] = val
                    else:
                        computed_taxes[key]['base'] += val['base']
                        computed_taxes[key]['amount'] += val['amount']
                    _logger.debug("[_on_change_lines_taxes] val['base']:" % \
                        (val['base']))
                    _logger.debug("[_on_change_lines_taxes] val['amount']" % \
                        (val['amount']))

        if self.currency:
            for key in computed_taxes:
                for field in ('base', 'amount'):
                    computed_taxes[key][field] = self.currency.round(
                        computed_taxes[key][field])
        tax_keys = []
        for tax in (self.taxes or []):
            if tax.manual:
                res['tax_amount'] += tax.amount or Decimal('0.0')
                continue
            key = (tax.base_code.id if tax.base_code else None, tax.base_sign,
                tax.tax_code.id if tax.tax_code else None, tax.tax_sign,
                tax.account.id if tax.account else None,
                tax.tax.id if tax.tax else None)
            if (key not in computed_taxes) or (key in tax_keys):
                res['taxes'].setdefault('remove', [])
                res['taxes']['remove'].append(tax.id)
                continue
            tax_keys.append(key)
            if self.currency:
                if not self.currency.is_zero(
                        computed_taxes[key]['base']
                        - (tax.base or Decimal('0.0'))):
                    res['tax_amount'] += computed_taxes[key]['amount']
                    res['taxes'].setdefault('update', [])
                    res['taxes']['update'].append({
                            'id': tax.id,
                            'amount': computed_taxes[key]['amount'],
                            'base': computed_taxes[key]['base'],
                            })
                else:
                    res['tax_amount'] += tax.amount or Decimal('0.0')
            else:
                if (computed_taxes[key]['base'] - (tax.base or Decimal('0.0'))
                        != Decimal('0.0')):
                    res['tax_amount'] += computed_taxes[key]['amount']
                    res['taxes'].setdefault('update', [])
                    res['taxes']['update'].append({
                        'id': tax.id,
                        'amount': computed_taxes[key]['amount'],
                        'base': computed_taxes[key]['base'],
                        })
                else:
                    res['tax_amount'] += tax.amount or Decimal('0.0')
        for key in computed_taxes:
            if key not in tax_keys:
                res['tax_amount'] += computed_taxes[key]['amount']
                res['taxes'].setdefault('add', [])
                value = InvoiceTax.default_get(InvoiceTax._fields.keys())
                value.update(computed_taxes[key])
                for field, Target in (
                        ('account', Account),
                        ('base_code', TaxCode),
                        ('tax_code', TaxCode),
                        ('tax', Tax),
                        ):
                    if value.get(field):
                        value[field + '.rec_name'] = \
                            Target(value[field]).rec_name
                res['taxes']['add'].append(value)

        # Begin : Management of tax included
        if self.currency:
            res['total_amount'] = self.currency.round(res['total_amount'])
            res['tax_amount'] = self.currency.round(res['tax_amount'])
            res['untaxed_amount'] = \
                self.currency.round(res['total_amount'] - res['tax_amount'])
        # End : Management of tax included
        _logger.debug('[_on_change_lines_taxes] End : res:%s', res)
        return res

    @classmethod
    def get_amount(cls, invoices, names):
        pool = Pool()
        InvoiceTax = pool.get('account.invoice.tax')
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        cursor = Transaction().cursor

        untaxed_amount = dict((i.id, _ZERO) for i in invoices)
        tax_amount = dict((i.id, _ZERO) for i in invoices)
        total_amount = dict((i.id, _ZERO) for i in invoices)

        type_name = cls.tax_amount._field.sql_type().base
        in_max = cursor.IN_MAX
        tax = InvoiceTax.__table__()
        for i in range(0, len(invoices), in_max):
            sub_ids = [i.id for i in invoices[i:i + in_max]]
            red_sql = reduce_ids(tax.invoice, sub_ids)
            cursor.execute(*tax.select(tax.invoice,
                    Coalesce(Sum(tax.amount), 0).as_(type_name),
                    where=red_sql,
                    group_by=tax.invoice))
            for invoice_id, sum_ in cursor.fetchall():
                # SQLite uses float for SUM
                if not isinstance(sum_, Decimal):
                    sum_ = Decimal(str(sum_))
                tax_amount[invoice_id] = sum_

        invoices_move = []
        invoices_no_move = []
        for invoice in invoices:
            if invoice.move:
                invoices_move.append(invoice)
            else:
                invoices_no_move.append(invoice)

        type_name = cls.total_amount._field.sql_type().base
        invoice = cls.__table__()
        move = Move.__table__()
        line = MoveLine.__table__()
        for i in range(0, len(invoices_move), in_max):
            sub_ids = [i.id for i in invoices_move[i:i + in_max]]
            red_sql = reduce_ids(invoice.id, sub_ids)
            cursor.execute(*invoice.join(move,
                    condition=invoice.move == move.id
                    ).join(line, condition=move.id == line.move
                    ).select(invoice.id,
                    Coalesce(Sum(line.debit - line.credit), 0).cast(type_name),
                    where=(invoice.account == line.account) & red_sql,
                    group_by=invoice.id))
            for invoice_id, sum_ in cursor.fetchall():
                # SQLite uses float for SUM
                if not isinstance(sum_, Decimal):
                    sum_ = Decimal(str(sum_))
                total_amount[invoice_id] = sum_

        for invoice in invoices_move:
            if invoice.type in ('in_invoice', 'out_credit_note'):
                total_amount[invoice.id] *= -1
            untaxed_amount[invoice.id] = (
                total_amount[invoice.id] - tax_amount[invoice.id])

        for invoice in invoices_no_move:
            # Begin : Management of tax included
            if invoice.price_type == 't_inc':
                total_amount[invoice.id] = sum(
                    (line.amount for line in invoice.lines
                        if line.type == 'line'), _ZERO)
                #untaxed_amount[invoice.id] = (
                #    total_amount[invoice.id] - tax_amount[invoice.id])
                untaxed_amount[invoice.id] = sum(
                    (line.amount_texc for line in invoice.lines
                        if line.type == 'line'), _ZERO)
            else:
                untaxed_amount[invoice.id] = sum(
                    (line.amount for line in invoice.lines
                        if line.type == 'line'), _ZERO)
                total_amount[invoice.id] = (
                    untaxed_amount[invoice.id] + tax_amount[invoice.id])
            # End : Management of tax included
        result = {
            'untaxed_amount': untaxed_amount,
            'tax_amount': tax_amount,
            'total_amount': total_amount,
            }
        for key in result.keys():
            if key not in names:
                del result[key]
        return result

    def get_tax_context(self):
        res = super(Invoice, self).get_tax_context()
        res['tax_included'] = True if self.price_type == 't_inc' else False
        return res

    def _credit(self):
        '''
        Add price_type in duplicated fields
        '''
        res = super(Invoice, self)._credit()
        for field in ('price_type',):
            res[field] = getattr(self, field)
        return res

class InvoiceLine(ModelSQL, ModelView):
    __name__ = 'account.invoice.line'

    amount_texc = fields.Function(fields.Numeric('Amount (tax excl.)',
        digits=(16, Eval('_parent_invoice', {}).get('currency_digits',
                    Eval('currency_digits', 2))),
        states={
            'invisible': Eval('_parent_sale', {}).get('price_type') == 't_exc',
            },),
        'get_amount_texc')

    @classmethod
    def __setup__(cls):
        super(InvoiceLine, cls).__setup__()
        if '_parent_invoice.price_type' not in cls.product.on_change:
            cls.product.on_change.append('_parent_invoice.price_type')

        if not getattr(cls.amount_texc, 'on_change_with', False):
            cls.amount_texc.on_change_with = cls.amount.on_change_with
        if '_parent_invoice.price_type' not in cls.amount_texc.on_change_with:
            cls.amount_texc.on_change_with.append('_parent_invoice.price_type')
        if 'taxes' not in cls.amount_texc.on_change_with:
            cls.amount_texc.on_change_with.append('taxes')
        if '_parent_invoice.party' not in cls.amount_texc.on_change_with:
            cls.amount_texc.on_change_with.append('_parent_invoice.party')


    def on_change_with_amount_texc(self):
        pool = Pool()
        Tax = pool.get('account.tax')

        if self.type == 'line':
            context = self.invoice.get_tax_context()
            if context.get('tax_included', False):
                base = 0
                currency = (self.invoice.currency if self.invoice
                    else self.currency)
                with Transaction().set_context(**context):
                    taxes = Tax.compute(self.taxes, self.unit_price,
                        self.quantity)
                for tax in taxes:
                    base += tax['base']
                #if currency:
                #    return currency.round(base)
                return base
        return Decimal('0.0')

    def get_amount_texc(self, name):
        _logger.debug('get_amount_texc:Begin')
        return self.on_change_with_amount_texc()

    def get_move_line(self):
        '''
        Return a list of move lines values for invoice line
        '''
        Currency = Pool().get('currency.currency')
        res = {}
        if self.type != 'line':
            return []
        res['description'] = self.description

        # Begin : Management of tax included
        if self.invoice.price_type == 't_inc':
            amt = self.amount_texc
        else:
            amt = self.amount
        # End : Management of tax included

        if self.invoice.currency != self.invoice.company.currency:
            with Transaction().set_context(date=self.invoice.currency_date):
                amount = Currency.compute(self.invoice.currency,
                    amt, self.invoice.company.currency)
            res['amount_second_currency'] = amt
            res['second_currency'] = self.invoice.currency.id
        else:
            amount = amt
            res['amount_second_currency'] = Decimal('0.0')
            res['second_currency'] = None
        if self.invoice.type in ('in_invoice', 'out_credit_note'):
            if amount >= Decimal('0.0'):
                res['debit'] = amount
                res['credit'] = Decimal('0.0')
            else:
                res['debit'] = Decimal('0.0')
                res['credit'] = - amount
                res['amount_second_currency'] = - res['amount_second_currency']
        else:
            if amount >= Decimal('0.0'):
                res['debit'] = Decimal('0.0')
                res['credit'] = amount
                res['amount_second_currency'] = - res['amount_second_currency']
            else:
                res['debit'] = - amount
                res['credit'] = Decimal('0.0')
        res['account'] = self.account.id
        res['party'] = self.invoice.party.id
        computed_taxes = self._compute_taxes()
        if computed_taxes:
            res['tax_lines'] = [('create', [tax for tax in computed_taxes])]
        return [res]
# -*- coding: utf-8 -*-
# This file is part of Sale B2BC Tryton Module.  The 
# COPYRIGHT file at the top level of this repository contains the
# full copyright notices and license terms.

import logging
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction

__all__ = ['Tax',]

__metaclass__ = PoolMeta

_logger = logging.getLogger(__name__)
_logger.setLevel(logging.INFO)


class Tax(ModelSQL, ModelView):
    '''
    Account Tax

    Type:
        percentage: 
            base = price_unit / (1 + rate)
            amount = base - price_unit
        fixed: 
            base = price_unit - amount
        none: tax = none
    '''
    __name__ = 'account.tax'

    def _process_tax(self, price_unit, tax_included=False):
        ''' 
        Overload the standard method to add the calculation of taxes from a 
        tax included price
        '''
        if not tax_included:
            return super(Tax, self)._process_tax(price_unit)
        
        base = price_unit
        if self.type == 'percentage':
            base = self.company.currency.round(price_unit / (1 + self.rate))
            amount = price_unit - base
        if self.type == 'fixed':
            base = price_unit - self.amount
            amount = self.amount
        return {
            'base': base,
            'amount': amount,
            'tax': self,
            }

    @classmethod
    def _unit_compute(cls, taxes, price_unit, tax_included=False):
        res = []
        
        if not tax_included:
            return super(Tax, cls)._unit_compute(taxes, price_unit)
        
        for tax in taxes:
            if tax.type != 'none':
                res.append(tax._process_tax(price_unit,
                    tax_included=tax_included))
            if len(tax.childs):
                res.extend(cls._unit_compute(tax.childs, price_unit,
                    tax_included=tax_included))
        return res
    
    @classmethod
    def compute(cls, taxes, price_unit, quantity):
        #[ADZ] log ...
        _logger.debug('[compute] Begin')
                
        tax_included = Transaction().context.get('tax_included', False)
        # If the price is tax excluded : Standard treatment ...
        if not tax_included:
            _logger.debug('[compute] Standard treatment ...')
            return super(Tax, cls).compute(taxes, price_unit, quantity)
        
        # ... else ...
        
        taxes = cls.sort_taxes(taxes)
        quantity = Decimal(str(quantity or 0.0))
        stotal_tinc = price_unit * quantity
        
        res = cls._unit_compute(taxes, stotal_tinc, tax_included=tax_included)
        #[ADZ] log ...
        _logger.debug('[compute] res:%s', res)
        return res




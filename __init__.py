# -*- coding: utf-8 -*-
# This file is part Sale B2BC module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.pool import Pool
from .product import *
from .shop import *
from .sale import *
from .invoice import *
from .tax import *
from .party import *

def register():
    Pool.register(
        Template,
        Product,
        SaleShop,
        Sale,
        SaleLine,
        Invoice,
        InvoiceLine,
        Tax,
        Party,
        module='sale_b2bc', type_='model')